# Algoritmo para Conversão de Base - Método Divisão Sucessivas

## Descrição

Este projeto tem o objetivo de fazer um código de conversão de número decimal para binário e vice-versa.

## Como utilizar o CONVERSOR DE BASE

Para usar a biblioteca adicione o `convert.h` no seu código:

```c
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "convert.h"
```

Para rodar exemplo, use o seguinte comando:

```bash
gcc ./src/main.c -I ./Include/ -o main
./main
```


## Autores
Esse algoritmo foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/11469689/avatar.png?width=400)  | Alice Gomes | [@alicag](https://gitlab.com/alicag) | [alices@alunos.utfpr.edu.br](mailto:alices@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/11328034/avatar.png?width=400)  | Júlia Salvi | [@JuliaSalvi](https://gitlab.com/JuliaSalvi) | [juliag.2022@alunos.utfpr.edu.br](mailto:juliag.2022@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/11253278/avatar.png?width=400)  | Maria Isabela | [@mariaisabelasn](https://gitlab.com/mariaisabelasn) | [mariaisabela@alunos.utfpr.edu.br](mailto:mariaisabela@alunos.utfpr.edu.br)




